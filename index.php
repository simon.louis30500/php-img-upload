<html>
   <body> 
      <form action = "" method = "POST" enctype = "multipart/form-data">
         <input type = "file" name = "image">
         <input type = "submit">
      </form>
      <?php
         $fichiers = scandir("images/");
         $dots = [".", ".."];
         echo "Fichiers enregistrés :<br><br>";
         foreach($fichiers as $file){
            if(!in_array($file, $dots)){
               echo $file . "<br>";
            }
         }
         if(isset($_FILES["image"])){
            $errors = array();
            $file_name = $_FILES["image"]["name"];
            $file_size = $_FILES["image"]["size"];
            $file_tmp = $_FILES["image"]["tmp_name"];
            $file_type = $_FILES["image"]["type"];
            $file_exploded = explode(".",$_FILES["image"]["name"]);
            $exploded_end = end($file_exploded);
            $file_extension = strtolower($exploded_end);
            $extensions= array("jpeg","jpg","png", "pdf");
            if(!in_array($file_extension, $extensions)){
               $error = "<br>Extension de fichier invalide, sélectionnez un fichier JPEG, JPG, PNG ou PDF.<br>";
            }
            if($file_size > 8097152){
               $error = "<br>Le fichier doit peser moins de 8 Mo.<br>";
            }
            if(empty($error)){
               move_uploaded_file("$file_tmp","images/".$_FILES["image"]["name"]);
               $size = getimagesize("images/".$file_name);
               echo "<br>Votre fichier a bien été envoyé, si c'est une image, la voici : <br><br>";
               if($file_extension === "pdf"){
                  echo "<embed src="."images/".$_FILES['image']['name']." height='800' width='800'/>";
                  echo "<br>";
               }
               else{
                  $width = $size[0];
                  $height = $size[1];
                  echo "<br>Largeur : " . $width . "<br>" . "Hauteur : " . $height . "<br>";
                  echo "<img src="."images/".$_FILES['image']['name']." height=" . $height . "width=" . $width . "/>";
               }
            }
            else
            {
               print_r($error);
            } 
         } 
      ?>
   </body>
</html>